import math

import torch as th
from torch import nn

from .model import rescale_module
from .utils import capture_init


# inherits from the Demucs model
class NoGT(nn.Module):
    @capture_init
    def __init__(self,
                 sources=1,
                 audio_channels=2,
                 channels=64,
                 depth=6,
                 rewrite=True,
                 glu=True,
                 upsample=False,
                 rescale=0.1,
                 kernel_size=8,
                 stride=4,
                 growth=2.,
                 lstm_layers=2,
                 context=3,
                 discriminative_channel_fraction=0.2,
                 samples_pre_valid_length=None):
        """
        Args:
            sources (int): number of sources to separate
            audio_channels (int): stereo or mono
            channels (int): first convolution channels
            depth (int): number of encoder/decoder layers
            rewrite (bool): add 1x1 convolution to each encoder layer
                and a convolution to each decoder layer.
                For the decoder layer, `context` gives the kernel size.
            glu (bool): use glu instead of ReLU
            upsample (bool): use linear upsampling with convolutions
                Wave-U-Net style, instead of transposed convolutions
            rescale (int): rescale initial weights of convolutions
                to get their standard deviation closer to `rescale`
            kernel_size (int): kernel size for convolutions
            stride (int): stride for convolutions
            growth (float): multiply (resp divide) number of channels by that
                for each layer of the encoder (resp decoder)
            lstm_layers (int): number of lstm layers, 0 = no lstm
            context (int): kernel size of the convolution in the
                decoder before the transposed convolution. If > 1,
                will provide some context from neighboring time
                steps.
        """

        super().__init__()
        self.lstm_layers = lstm_layers
        self.audio_channels = audio_channels
        self.sources = sources
        self.kernel_size = kernel_size
        self.context = context
        self.stride = stride
        self.depth = depth
        self.upsample = upsample
        self.channels = channels
        self.samples = self.valid_length(samples_pre_valid_length)

        self.encoder = nn.ModuleList()
        self.decoder = nn.ModuleList()

        self.discriminative_channel_fraction = discriminative_channel_fraction

        self.discriminative_channel_indices = []

        if glu:
            activation = nn.GLU(dim=1)
            ch_scale = 2
        else:
            activation = nn.ReLU()
            ch_scale = 1
        in_channels = audio_channels
        dropout_probability = 0.1
        for index in range(depth):
            encode = []
            # encode += [nn.Conv1d(in_channels, channels, kernel_size, stride), nn.ReLU(),
            #            nn.Dropout(p=dropout_probability)]
            encode += [nn.Conv1d(in_channels, channels, kernel_size, stride), nn.ReLU()]
            if rewrite:
                # encode += [nn.Conv1d(channels, ch_scale * channels, 1), activation, nn.Dropout(p=dropout_probability)]
                encode += [nn.Conv1d(channels, ch_scale * channels, 1), activation]
            self.encoder.append(nn.Sequential(*encode))

            self.discriminative_channel_no = int(channels * self.discriminative_channel_fraction)

            decode = []
            if index > 0:
                out_channels = in_channels
            else:
                if upsample:
                    out_channels = channels
                else:
                    out_channels = sources * audio_channels
            if rewrite:
                # decode += [nn.Conv1d(channels, ch_scale * channels, context), activation,
                #            nn.Dropout(p=dropout_probability)]
                decode += [nn.Conv1d(channels, ch_scale * channels, context), activation]
            if upsample:
                decode += [
                    nn.Conv1d(channels, out_channels, kernel_size, stride=1),
                ]
            else:
                # decode += [nn.ConvTranspose1d(channels, out_channels, kernel_size, stride),
                #            nn.Dropout(p=dropout_probability)]
                decode += [nn.ConvTranspose1d(channels, out_channels, kernel_size, stride)]
            if index > 0:
                decode.append(nn.ReLU())
            self.decoder.insert(0, nn.Sequential(*decode))
            in_channels = channels
            channels = int(growth * channels)

        kernel_size = 8
        stride = 4
        self.discriminative = nn.Sequential(
            nn.Conv1d(self.discriminative_channel_no, 40, kernel_size, stride),
            nn.ReLU(),
            nn.Conv1d(40, 10, kernel_size, stride),
            nn.ReLU(),
            nn.Conv1d(10, 1, kernel_size, stride),
            nn.ReLU(),
            nn.Linear(12, 1),
        )

        if rescale:
            rescale_module(self, reference=rescale)

    # used to encode and decode only a subset of the channels
    def extract_part(self, mix, discriminative_channel_indices):
        x = mix
        saved = [x]

        for encode in self.encoder:
            x = encode(x)
            saved.append(x)

        # take only some channels of the encoded format
        # to only take the channels containing the instrument I want

        # create a mask of 0s
        mask = th.zeros(x.shape[0], x.shape[1], x.shape[2]).cuda()
        # set it to 1 for the channels we want
        mask[:, discriminative_channel_indices, :] = 1
        # multiply x by the mask
        x = x * mask

        for decode in self.decoder:
            x = decode(x)

        return x

    # encode audio and run it through the discriminative network
    def forward_encode_and_discriminative(self, mix):
        x = mix
        saved = [x]

        for encode in self.encoder:
            x = encode(x)
            saved.append(x)

        # take only some channels of the encoded format
        x = th.index_select(x, dim=1, index=self.discriminative_channel_indices)

        # make the input into the discriminative network a consistent length
        # sets it to the length of the training samples
        desired_length = 863
        if x.shape[2] < desired_length:
            x = th.nn.functional.pad(x, pad=(0, 0, desired_length - x.shape[2]))
        if x.shape[2] > desired_length:
            x = x[..., :desired_length]

        x = self.discriminative(x)
        x = x.squeeze()
        return x

    # encode and decode audio in full
    def forward(self, mix):
        x = mix
        saved = [x]
        for encode in self.encoder:
            x = encode(x)
            saved.append(x)
        for decode in self.decoder:
            x = decode(x)
        return x

    # return the encoded channels for most active channels selection
    def return_encoded_channels(self, mix):
        x = mix
        saved = [x]

        for encode in self.encoder:
            x = encode(x)
            saved.append(x)

        return x

    def valid_length(self, length):
        """
        Return the nearest valid length to use with the model so that
        there is no time steps left over in a convolutions, e.g. for all
        layers, size of the input - kernel_size % stride = 0.

        If the mixture has a valid length, the estimated sources
        will have exactly the same length when context = 1. If context > 1,
        the two signals can be center trimmed to match.

        For training, extracts should have a valid length.For evaluation
        on full tracks we recommend passing `pad = True` to :method:`forward`.
        """
        for _ in range(self.depth):
            if self.upsample:
                length = math.ceil(length / self.stride) + self.kernel_size - 1
            else:
                length = math.ceil((length - self.kernel_size) / self.stride) + 1
            length = max(1, length)
            length += self.context - 1
        for _ in range(self.depth):
            if self.upsample:
                length = length * self.stride + self.kernel_size - 1
            else:
                length = (length - 1) * self.stride + self.kernel_size

        return int(length)
