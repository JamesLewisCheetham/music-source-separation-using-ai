# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import sys

import numpy as np
import torch as th
import tqdm
from torch.utils.data import DataLoader

from .utils import center_trim


# https://stackoverflow.com/questions/44230312/fastest-way-to-create-numpy-2d-array-of-indices
def indices_array(n):
    r = np.arange(n)
    out = np.empty(n, dtype=int)
    out[:] = r[:]
    return out


class ModelTrainer:
    def __init__(self,
                 dataset_train,
                 dataset_validation,
                 model,
                 criterion_autoencoder,
                 criterion_discriminative,
                 optimizer,
                 optimizer_discriminative,
                 augment,
                 device="cpu",
                 seed=None,
                 workers=4,
                 world_size=1,
                 batch_size=16,
                 discriminative_weighting=5,
                 samples=None):

        self.model = model
        self.criterion_autoencoder = criterion_autoencoder
        self.criterion_discriminative = criterion_discriminative
        self.optimizer = optimizer
        self.optimizer_discriminative = optimizer_discriminative
        self.augment = augment
        self.device = device
        self.seed = seed
        self.workers = workers
        self.world_size = world_size
        self.batch_size = batch_size
        self.discriminative_weighting = discriminative_weighting
        self.samples = samples

        self.train_autoencoder_initial_epochs = 0
        self.train_autoencoder_initial_losses = []

        self.train_discriminative_epochs = 0
        self.train_discriminative_losses = []

        self.train_autoencoder_finetune_epochs = 0
        self.train_autoencoder_finetune_losses = []

        # raise error if attempt to use distributed training as it is not supported
        if world_size > 1:
            raise Exception("World size >1 unsupported. See Demucs code for how to implement this.")
        else:
            self.data_loader_train = DataLoader(dataset_train, batch_size=batch_size, num_workers=workers,
                                                shuffle=True)
            self.data_loader_validation = DataLoader(dataset_validation, batch_size=batch_size, num_workers=workers,
                                                     shuffle=True)

    # stage 1 training of the autoencoder
    def train_autoencoder_initial(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss = 0
        current_loss = 0
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Training autoencoder",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            stream = stream.unsqueeze(1)
            source = self.augment(stream)
            # remove a dimension from the source
            # holdover from when stems were used, needed to remain util now for augment to work properly
            mix = source.squeeze(dim=1)

            # Run the mix through the encoder/decoder to get an output
            estimate = self.model(mix)
            #  makes sure the mix and output tensors are the same size
            mix = center_trim(mix, estimate)
            # Calculate the loss
            loss_autoencoder = self.criterion_autoencoder(estimate, mix)

            loss_autoencoder.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            total_loss += loss_autoencoder.item()
            current_loss = total_loss / (1 + idx)
            tq.set_postfix(autoencoder_loss=f"{current_loss:.4f}")

            # free some space before next round
            del stream, source, mix, estimate, loss_autoencoder

        return current_loss

    # stage 1 validation of the autoencoder
    def validate_autoencoder_initial(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss = 0
        current_loss = 0
        tq = tqdm.tqdm(self.data_loader_validation,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Validating autoencoder",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):

            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            mix = stream

            # Run the mix through the encoder/decoder to get an output
            estimate = self.model(mix)
            #  makes sure the mix and output tensors are the same size
            mix = center_trim(mix, estimate)
            # Calculate the loss
            loss_autoencoder = self.criterion_autoencoder(estimate, mix)

            total_loss += loss_autoencoder.item()
            current_loss = total_loss / (1 + idx)
            tq.set_postfix(autoencoder_loss=f"{current_loss:.4f}")

            # free some space before next round
            del stream, mix, estimate, loss_autoencoder

        return current_loss

    # stage 3 training of the discriminative network
    def train_discriminative(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_discriminative = 0
        current_loss = 0
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Training discriminative network",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            stream = stream.unsqueeze(1)
            source = self.augment(stream)
            # remove a dimension from the source
            # holdover from when stems were used, needed to remain util now for augment to work properly
            mix = source.squeeze(dim=1)

            # the cross entropy criterion uses 1 to represent violin
            if tensor_and_category["category"] == ["violin"]:
                target = th.tensor(1).float().cuda()
            else:
                target = th.tensor(0).float().cuda()

            category_estimate = self.model.forward_encode_and_discriminative(mix)
            loss_discriminative = self.criterion_discriminative(category_estimate, target)
            # freeze encoder layers and unfreeze them after backpropagation
            for layer in self.model.encoder:
                layer.requires_grad = False
            loss_discriminative.backward()
            self.optimizer_discriminative.step()
            self.optimizer_discriminative.zero_grad()
            for layer in self.model.encoder:
                layer.requires_grad = True

            total_loss_discriminative += loss_discriminative.item()
            current_loss = total_loss_discriminative / (1 + idx)
            tq.set_postfix(discriminative_loss=f"{current_loss:.4f}")

            # free some space before next round
            del stream, source, mix, loss_discriminative

        return current_loss

    # stage 3 validation of the discriminative network
    def validate_discriminative(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_discriminative = 0
        current_loss = 0
        tq = tqdm.tqdm(self.data_loader_validation,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Validating discriminative network",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            mix = stream

            # the cross entropy criterion uses class attributes (a number representing each class), not one hot
            # encoding
            if tensor_and_category["category"] == ["violin"]:
                target = th.tensor(1).float().cuda()
            else:
                target = th.tensor(0).float().cuda()
            category_estimate = self.model.forward_encode_and_discriminative(mix)
            loss_discriminative = self.criterion_discriminative(category_estimate, target)

            total_loss_discriminative += loss_discriminative.item()
            current_loss = total_loss_discriminative / (1 + idx)
            tq.set_postfix(discriminative_loss=f"{current_loss:.4f}")

            # free some space before next round
            del stream, mix, loss_discriminative

        return current_loss

    # stage 4 training of the autoencoder
    def train_autoencoder_finetune(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_autoencoder = 0
        total_loss_autoencoder_modified = 0
        total_loss_addition = 0
        current_loss_autoencoder_modified = 0
        current_loss_autoencoder = 0
        current_loss_addition = 0
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Fine tuning autoencoder channels",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            stream = stream.unsqueeze(1)
            source = self.augment(stream)
            # remove a dimension from the source
            # holdover from when stems were used, needed to remain util now for augment to work properly
            mix = source.squeeze(dim=1)

            estimate = self.model(mix)
            mix_centre_trimmed = center_trim(mix, estimate)
            loss_autoencoder = self.criterion_autoencoder(estimate, mix_centre_trimmed)

            category_estimate = self.model.forward_encode_and_discriminative(mix)
            predicted_probability = th.sigmoid(category_estimate)
            predicted_probability_float = float(predicted_probability)

            loss_addition = self.criterion_autoencoder(predicted_probability, th.tensor(1).cuda()) * self.discriminative_weighting

            loss_autoencoder_modified = loss_autoencoder + loss_addition

            for layer in self.model.discriminative:
                layer.requires_grad = False

            loss_autoencoder_modified.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            total_loss_autoencoder += loss_autoencoder.item()
            current_loss_autoencoder = total_loss_autoencoder / (1 + idx)
            total_loss_addition += loss_addition.item()
            current_loss_addition = total_loss_addition / (1 + idx)
            total_loss_autoencoder_modified += loss_autoencoder_modified.item()
            current_loss_autoencoder_modified = total_loss_autoencoder_modified / (1 + idx)
            tq.set_postfix(
                loss=f" Autoencoder Modified: {current_loss_autoencoder_modified:.4f} "
                     f"(Autoencoder: {current_loss_autoencoder:.4f} + "
                     f"Loss Addition: {current_loss_addition:.4f}), "
                     f"Violin Predicted Probability: {predicted_probability_float:.4f}")

            # free some space before next round
            del stream, source, mix, estimate, loss_autoencoder, loss_autoencoder_modified, loss_addition

        return current_loss_autoencoder_modified, current_loss_autoencoder, current_loss_addition

    # stage 4 validation of the autoencoder
    def validate_autoencoder_finetune(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_autoencoder = 0
        total_loss_autoencoder_modified = 0
        total_loss_addition = 0
        current_loss_autoencoder_modified = 0
        current_loss_autoencoder = 0
        current_loss_addition = 0
        tq = tqdm.tqdm(self.data_loader_validation,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Validating",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            mix = stream

            estimate = self.model(mix)
            mix_centre_trimmed = center_trim(mix, estimate)
            loss_autoencoder = self.criterion_autoencoder(estimate, mix_centre_trimmed)

            category_estimate = self.model.forward_encode_and_discriminative(mix)
            predicted_probability = th.sigmoid(category_estimate)
            predicted_probability_float = float(predicted_probability)

            loss_addition = self.criterion_autoencoder(predicted_probability,
                                                       th.tensor(1).cuda()) * self.discriminative_weighting

            loss_autoencoder_modified = loss_autoencoder + loss_addition

            total_loss_autoencoder += loss_autoencoder.item()
            current_loss_autoencoder = total_loss_autoencoder / (1 + idx)
            total_loss_addition += loss_addition.item()
            current_loss_addition = total_loss_addition / (1 + idx)
            total_loss_autoencoder_modified += loss_autoencoder_modified.item()
            current_loss_autoencoder_modified = total_loss_autoencoder_modified / (1 + idx)
            tq.set_postfix(
                loss=f" Autoencoder Modified: {current_loss_autoencoder_modified:.4f} "
                     f"(Autoencoder: {current_loss_autoencoder:.4f} + "
                     f"Loss Addition: {current_loss_addition:.4f}), "
                     f"Violin Predicted Probability: {predicted_probability_float:.4f}")

            # free some space before next round
            del stream, mix, estimate, loss_autoencoder, loss_autoencoder_modified, loss_addition

        return current_loss_autoencoder_modified, current_loss_autoencoder, current_loss_addition

    # stage 2 selection of the most active channels
    def set_most_active_channels(self, discriminative_channel_fraction):
        print("Selecting most active channels")
        violin_tracks_processed = 0
        output_cumulative = None
        # find the most activate channels
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=120,
                       desc=f"Selecting most activate channels",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            if tensor_and_category["category"] == ['violin']:
                stream = tensor_and_category["tensor"]
                stream = stream.to(self.device)
                stream = stream.unsqueeze(1)
                source = self.augment(stream)
                # remove a dimension from the source
                # holdover from when stems were used, needed to remain util now for augment to work properly
                mix = source.squeeze(dim=1)
                output = self.model.return_encoded_channels(mix).clone().detach()

                output = output.abs()
                output = th.mean(th.squeeze(output), dim=1)

                if output_cumulative is None:
                    output_cumulative = output
                else:
                    output_cumulative = output_cumulative + output
                violin_tracks_processed += 1

                # free some space before next round
                del tensor_and_category, stream, source, mix, output

        average_output = th.div(output_cumulative, violin_tracks_processed)

        no_channels = output_cumulative.size()[0]
        no_channels_to_take = int(no_channels * discriminative_channel_fraction)

        _, discriminative_channel_indices = average_output.topk(no_channels_to_take)
        self.model.discriminative_channel_indices = discriminative_channel_indices
        print("Selected " + str(no_channels_to_take) + " discriminative channels")

    # experimental method 1, training
    def train_autoencoder_new(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_autoencoder = 0
        total_loss_autoencoder_modified = 0
        total_loss_addition = 0
        current_loss_autoencoder_modified = 0
        current_loss_autoencoder = 0
        current_loss_addition = 0

        new_lr = 3e-4/epoch
        self.optimizer = th.optim.Adam(self.model.parameters(), new_lr)
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Tuning autoencoder channels",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):

            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            stream = stream.unsqueeze(1)
            source = self.augment(stream)
            # remove a dimension from the source
            # holdover from when stems were used, needed to remain util now for augment to work properly
            mix = source.squeeze(dim=1)

            estimate = self.model(mix)
            mix = center_trim(mix, estimate)
            loss_autoencoder = self.criterion_autoencoder(estimate, mix)

            # get the sum of the violin channels
            channels = self.model.return_encoded_channels(mix).clone().detach()
            violin_channel_indices = self.model.discriminative_channel_indices
            violin_channels = th.index_select(channels, dim=1, index=violin_channel_indices)
            violin_channels_sum = violin_channels.abs().sum()

            # get the sum of the other channels
            violin_channel_indices_array = violin_channel_indices.cpu().numpy()
            full_indices = indices_array(channels.shape[1])
            other_channels_indices = th.tensor(full_indices[~np.in1d(full_indices, violin_channel_indices_array)],
                                               dtype=th.int64).cuda()
            other_channels = th.index_select(channels, dim=1, index=other_channels_indices)
            other_channels_sum = other_channels.abs().sum()

            loss_addition_weighting = 0.0001
            if tensor_and_category["category"] == ['violin']:
                loss_addition = other_channels_sum * loss_addition_weighting
            else:
                loss_addition = violin_channels_sum * loss_addition_weighting

            loss_autoencoder_modified = loss_autoencoder + loss_addition

            loss_autoencoder_modified.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            total_loss_autoencoder += loss_autoencoder.item()
            current_loss_autoencoder = total_loss_autoencoder / (1 + idx)
            total_loss_addition += loss_addition.item()
            current_loss_addition = total_loss_addition / (1 + idx)
            total_loss_autoencoder_modified += loss_autoencoder_modified.item()
            current_loss_autoencoder_modified = total_loss_autoencoder_modified / (1 + idx)
            tq.set_postfix(
                loss=f" Autoencoder Modified: {current_loss_autoencoder_modified:.4f} "
                     f"(Autoencoder: {current_loss_autoencoder:.4f} + "
                     f"Loss addition: {current_loss_addition:.4f}), ")

            # free some space before next round
            del stream, source, mix, estimate, loss_autoencoder, loss_autoencoder_modified

        return current_loss_autoencoder_modified, current_loss_autoencoder, current_loss_addition

    # experimental method 1, validation
    def validate_autoencoder_new(self, epoch, total_epochs):
        epoch = epoch + 1
        total_loss_autoencoder = 0
        total_loss_autoencoder_modified = 0
        total_loss_addition = 0
        current_loss_autoencoder_modified = 0
        current_loss_autoencoder = 0
        current_loss_addition = 0
        tq = tqdm.tqdm(self.data_loader_validation,
                       ncols=200,
                       desc=f"[{epoch:03d}/{total_epochs:03d}] Validating fine tuning of autoencoder channels",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            if len(stream) < self.batch_size:
                # skip uncomplete batch for augment.Remix to work properly
                continue
            stream = stream.to(self.device)
            mix = stream

            estimate = self.model(mix)
            mix = center_trim(mix, estimate)
            loss_autoencoder = self.criterion_autoencoder(estimate, mix)

            channels = self.model.return_encoded_channels(mix).clone().detach()
            violin_channel_indices = self.model.discriminative_channel_indices
            violin_channels = th.index_select(channels, dim=1, index=violin_channel_indices)
            violin_channels_sum = violin_channels.abs().sum()

            violin_channel_indices_array = violin_channel_indices.cpu().numpy()
            full_indices = indices_array(channels.shape[1])
            other_channels_indices = th.tensor(full_indices[~np.in1d(full_indices, violin_channel_indices_array)],
                                               dtype=th.int64).cuda()
            other_channels = th.index_select(channels, dim=1, index=other_channels_indices)
            other_channels_sum = other_channels.abs().sum()

            loss_addition_weighting = 0.0001
            if tensor_and_category["category"] == ['violin']:
                loss_addition = other_channels_sum * loss_addition_weighting
            else:
                loss_addition = violin_channels_sum * loss_addition_weighting

            loss_autoencoder_modified = loss_autoencoder + loss_addition

            total_loss_autoencoder += loss_autoencoder.item()
            current_loss_autoencoder = total_loss_autoencoder / (1 + idx)
            total_loss_addition += loss_addition.item()
            current_loss_addition = total_loss_addition / (1 + idx)
            total_loss_autoencoder_modified += loss_autoencoder_modified.item()
            current_loss_autoencoder_modified = total_loss_autoencoder_modified / (1 + idx)
            tq.set_postfix(
                loss=f" Autoencoder Modified: {current_loss_autoencoder_modified:.4f} "
                     f"(Autoencoder: {current_loss_autoencoder:.4f} + "
                     f"Loss addition: {current_loss_addition:.4f}), ")

            # free some space before next round
            del stream, mix, estimate, loss_autoencoder, loss_autoencoder_modified

        return current_loss_autoencoder_modified, current_loss_autoencoder, current_loss_addition

    # experimental method 2
    def select_most_distinct_violin_channels(self, discriminative_channel_fraction):
        print("Selecting most active channels")
        violin_tracks_processed = 0
        other_tracks_processed = 0
        violin_output_cumulative = None
        other_output_cumulative = None
        # find the most activate channels
        tq = tqdm.tqdm(self.data_loader_train,
                       ncols=120,
                       desc=f"Selecting most distinct violin channels",
                       leave=False,
                       file=sys.stdout,
                       unit=" batch")
        for idx, tensor_and_category in enumerate(tq):
            stream = tensor_and_category["tensor"]
            stream = stream.to(self.device)
            stream = stream.unsqueeze(1)
            source = self.augment(stream)
            # remove a dimension from the source
            # holdover from when stems were used, needed to remain util now for augment to work properly
            mix = source.squeeze(dim=1)
            output = self.model.return_encoded_channels(mix).clone().detach()

            output = output.abs()
            output = th.mean(th.squeeze(output), dim=1)
            if tensor_and_category["category"] == ['violin']:
                if violin_output_cumulative is None:
                    violin_output_cumulative = output
                else:
                    violin_output_cumulative = violin_output_cumulative + output
                violin_tracks_processed += 1

            if tensor_and_category["category"] == ['other']:
                if violin_output_cumulative is None:
                    other_output_cumulative = output
                else:
                    other_output_cumulative = other_output_cumulative + output
                other_tracks_processed += 1

            # free some space before next round
            del tensor_and_category, stream, source, mix, output

        average_output_violin = th.div(violin_output_cumulative, violin_tracks_processed)

        average_output_other = th.div(other_output_cumulative, other_tracks_processed)

        no_channels = violin_output_cumulative.size()[0]
        no_channels_to_take = int(no_channels * discriminative_channel_fraction)

        violin_minus_other = average_output_violin - average_output_other

        _, discriminative_channel_indices = violin_minus_other.topk(no_channels_to_take)
        self.model.discriminative_channel_indices = discriminative_channel_indices
        print("Selected " + str(no_channels_to_take) + " discriminative channels")
