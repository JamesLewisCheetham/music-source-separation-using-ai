# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import json
import os
import sys
import time
from dataclasses import dataclass, field
from fractions import Fraction

import torch as th
from torch import distributed, nn

from .augment import FlipChannels, FlipSign, Remix, Shift
from .compressed import build_musdb_metadata, get_discriminative_tracks, DiscriminativeSet
from .model import Demucs
from .model_nogt import NoGT
from .parser import get_name, get_parser
from .raw import Rawset
from .tasnet import ConvTasNet
from .train_nogt import ModelTrainer
from .utils import human_seconds, load_model, sizeof_fmt


@dataclass
class SavedState:
    metrics: list = field(default_factory=list)
    last_state: dict = None
    best_state: dict = None
    optimizer: dict = None
    discriminative_channels_indices: list = None


def main():
    # parse the command line args
    parser = get_parser()
    args = parser.parse_args()
    name = get_name(parser, args)
    print(f"Experiment {name}")

    # set up directories
    args.logs.mkdir(exist_ok=True)
    metrics_path = args.logs / f"{name}.json"
    args.checkpoints.mkdir(exist_ok=True, parents=True)
    args.models.mkdir(exist_ok=True, parents=True)

    # assign the device based on if a GPU is used
    # however, this is a holdover from previous code, and code does require reworking if a CPU is used
    # as some variables are initialised and sent directly to the GPU
    if args.device is None:
        device = "cpu"
        if th.cuda.is_available():
            device = "cuda"
    else:
        device = args.device

    # Set the pytorch seed
    th.manual_seed(args.seed)
    # Prevents too many threads to be started when running `museval` as it can be quite
    # inefficient on NUMA architectures.
    os.environ["OMP_NUM_THREADS"] = "1"

    # used for distributed training across multiple systems
    # no longer supported however due to the change I have made
    if args.world_size > 1:
        if device != "cuda" and args.rank == 0:
            print("Error: distributed training is only available with cuda device", file=sys.stderr)
            sys.exit(1)
        th.cuda.set_device(args.rank % th.cuda.device_count())
        distributed.init_process_group(backend="nccl",
                                       init_method="tcp://" + args.master,
                                       rank=args.rank,
                                       world_size=args.world_size)

    # to reload from checkpoint for debugging
    checkpoint = args.checkpoints / f"{name}.th"
    if args.restart and checkpoint.exists():
        checkpoint.unlink()

    # initialise the model
    if args.test:
        args.epochs = 1
        args.repeat = 0
        model = load_model(args.models / args.test)
    elif args.tasnet:
        model = ConvTasNet(audio_channels=args.audio_channels, X=args.X)
    elif args.nogt:
        model = NoGT(
            audio_channels=args.audio_channels,
            channels=args.channels,
            context=args.context,
            depth=args.depth,
            glu=args.glu,
            growth=args.growth,
            kernel_size=args.kernel_size,
            lstm_layers=args.lstm_layers,
            rescale=args.rescale,
            rewrite=args.rewrite,
            sources=1,
            stride=args.conv_stride,
            upsample=args.upsample,
            discriminative_channel_fraction=args.d_c_f,
            samples_pre_valid_length=args.samples,
        )
    else:
        model = Demucs(
            audio_channels=args.audio_channels,
            channels=args.channels,
            context=args.context,
            depth=args.depth,
            glu=args.glu,
            growth=args.growth,
            kernel_size=args.kernel_size,
            lstm_layers=args.lstm_layers,
            rescale=args.rescale,
            rewrite=args.rewrite,
            sources=4,
            stride=args.conv_stride,
            upsample=args.upsample,
        )
    model.to(device)
    # optional printing of model parameters
    if args.show:
        print(model)
        size = sizeof_fmt(4 * sum(p.numel() for p in model.parameters()))
        print(f"Model size {size}")
        return

    # Setting number of samples so that all convolution windows are full.
    # Prevents hard to debug mistake with the prediction being shifted compared
    # to the input mixture.
    samples = model.valid_length(args.samples)
    print(f"Number of training samples adjusted to {samples}")

    # set the optimizers
    optimizer = th.optim.Adam(model.parameters(), lr=args.lr, weight_decay=0.1e-4)
    optimizer_discriminative = th.optim.Adam(model.parameters(), lr=args.lr)

    optimizer = th.optim.Adam(model.parameters(), lr=args.lr)
    optimizer_discriminative = th.optim.Adam(model.parameters(), lr=args.lr)

    # optional augmentations
    if args.augment:
        augment = nn.Sequential(FlipSign(), FlipChannels(), Shift(args.data_stride),
                                Remix(group_size=args.remix_group_size)).to(device)
    else:
        augment = Shift(args.data_stride)

    # choice of loss criterion
    if args.mse:
        criterion = nn.MSELoss()
    else:
        criterion = nn.L1Loss()

    # loading the datasets
    # optional choice to use raw data to speed up training
    if args.raw:
        train_set = Rawset(args.raw / "train",
                           samples=samples + args.data_stride,
                           channels=args.audio_channels,
                           stride=args.data_stride)
        valid_set = Rawset(args.raw / "validation",
                           samples=samples + args.data_stride,
                           channels=args.audio_channels,
                           stride=args.data_stride)
    else:
        if not args.metadata.is_file() and args.rank == 0 and args.musdb is not None:
            build_musdb_metadata(args.metadata, args.musdb, args.workers)
        if args.world_size > 1:
            distributed.barrier()
        duration = Fraction(samples + args.data_stride, args.samplerate)
        stride = Fraction(args.data_stride, args.samplerate)
        train_set = DiscriminativeSet(get_discriminative_tracks(args.dataset_nogt, "train"),
                                      duration=duration,
                                      stride=stride,
                                      samplerate=args.samplerate,
                                      channels=args.audio_channels)
        valid_set = DiscriminativeSet(get_discriminative_tracks(args.dataset_nogt, "validation"),
                                      duration=duration,
                                      stride=stride,
                                      samplerate=args.samplerate,
                                      channels=args.audio_channels)

    # setting the criterion for the discriminative network
    criterion_discriminative = nn.BCEWithLogitsLoss()

    # initialise the trainer object and parameters
    trainer = ModelTrainer(
        dataset_train=train_set,
        dataset_validation=valid_set,
        model=model,
        criterion_autoencoder=criterion,
        criterion_discriminative=criterion_discriminative,
        optimizer=optimizer,
        optimizer_discriminative=optimizer_discriminative,
        augment=augment,
        device=device,
        seed=args.seed,
        workers=args.workers,
        world_size=args.world_size,
        batch_size=args.batch_size,
        discriminative_weighting=args.d_l_w)

    # set the model to training mode
    # may be unnecessary as model should be set to training, but no downside to this
    trainer.model.train()
    # try to load a saved checkpoint
    try:
        saved = th.load(checkpoint, map_location='cpu')
    except IOError:
        saved = SavedState()
    else:
        trainer.model.load_state_dict(saved.last_state)
        trainer.optimizer.load_state_dict(saved.optimizer)
        trainer.model.discriminative_channel_indices = saved.discriminative_channel_indices.cuda()

    # parse argument to get epochs for each stage
    nogt_epochs = args.nogt_e
    epochs_autoencoder_initial = int(nogt_epochs[0])
    boolean_select_most_active = bool(nogt_epochs[1])
    epochs_discriminative = int(nogt_epochs[2])
    epochs_autoencoder_finetune = int(nogt_epochs[3])

    # to allow restarting from a checkpoint to occur
    # for debugging only though as best loss is not correctly reinitialised
    epochs_autoencoder_initial_completed = 0
    select_most_active_completed = False
    epochs_discriminative_completed = 0
    epochs_autoencoder_finetune_completed = 0
    for epoch, metrics in enumerate(saved.metrics):
        if metrics['stage'] != "selecting_most_active_channels":
            pass
        # counting existing training iterations performed
        if metrics['stage'] == "training_autoencoder_initial":
            epochs_autoencoder_initial_completed += 1
        if metrics['stage'] == "selecting_most_active_channels":
            select_most_active_completed = True
        if metrics['stage'] == "training_discriminative":
            epochs_discriminative_completed += 1
        if metrics['stage'] == "training_autoencoder_finetune":
            epochs_autoencoder_finetune_completed += 1

    # begin timer
    total_begin = time.time()

    # training section
    # main training is listed after the more experimental training techniques

    # one of the experimental training techniques that was not extensively tested
    if args.n_t2:
        print("Training nt2")
        # stage 1
        autoencoder_initial_best_loss = float("inf")
        print("Training Autoencoder Initial")
        for epoch in range(epochs_autoencoder_initial_completed, epochs_autoencoder_initial):
            begin = time.time()
            trainer.model.train()
            train_loss = trainer.train_autoencoder_initial(epoch, epochs_autoencoder_initial)
            trainer.model.eval()
            valid_loss = trainer.validate_autoencoder_initial(epoch, epochs_autoencoder_initial)

            duration = time.time() - begin

            if valid_loss < autoencoder_initial_best_loss:
                autoencoder_initial_best_loss = valid_loss
                saved.best_state = {
                    key: value.to("cpu").clone()
                    for key, value in trainer.model.state_dict().items()
                }
            saved.metrics.append({
                "stage": "training_autoencoder_initial",
                "train": train_loss,
                "valid": valid_loss,
                "best": autoencoder_initial_best_loss,
                "duration": duration
            })
            json.dump(saved.metrics, open(metrics_path, "w"))
            saved.last_state = trainer.model.state_dict()
            saved.optimizer = trainer.optimizer.state_dict()

            th.save(saved, checkpoint)

            print(f"Epoch {epoch + 1:03d}: "
                  f"train={train_loss:.8f} valid={valid_loss:.8f} best={autoencoder_initial_best_loss:.4f} "
                  f"duration={human_seconds(duration)}")

        trainer.model.load_state_dict(saved.best_state)

        # stage 2
        # this only needs to run once as it's not training, only determining the most active channels at the time
        if boolean_select_most_active == 1 and not select_most_active_completed:
            trainer.select_most_distinct_violin_channels(args.d_c_f)
            saved.discriminative_channel_indices = trainer.model.discriminative_channel_indices
            saved.metrics.append({
                "stage": "selecting_most_active_channels",
            })

    # one of the experimental training techniques that was not extensively tested
    elif args.n_t:
        # stage 1
        autoencoder_initial_best_loss = float("inf")
        print("Training Autoencoder Initial")
        for epoch in range(epochs_autoencoder_initial_completed, epochs_autoencoder_initial):
            begin = time.time()
            trainer.model.train()
            train_loss = trainer.train_autoencoder_initial(epoch, epochs_autoencoder_initial)
            trainer.model.eval()
            valid_loss = trainer.validate_autoencoder_initial(epoch, epochs_autoencoder_initial)

            duration = time.time() - begin

            if valid_loss < autoencoder_initial_best_loss:
                autoencoder_initial_best_loss = valid_loss
                saved.best_state = {
                    key: value.to("cpu").clone()
                    for key, value in trainer.model.state_dict().items()
                }
            saved.metrics.append({
                "stage": "training_autoencoder_initial",
                "train": train_loss,
                "valid": valid_loss,
                "best": autoencoder_initial_best_loss,
                "duration": duration
            })
            json.dump(saved.metrics, open(metrics_path, "w"))
            saved.last_state = trainer.model.state_dict()
            saved.optimizer = trainer.optimizer.state_dict()

            th.save(saved, checkpoint)

            print(f"Epoch {epoch + 1:03d}: "
                  f"train={train_loss:.8f} valid={valid_loss:.8f} best={autoencoder_initial_best_loss:.4f} "
                  f"duration={human_seconds(duration)}")

        if epochs_autoencoder_initial > 0:
            trainer.model.load_state_dict(saved.best_state)

        # stage 2
        # this only needs to run once as it's not training, only determining the most active channels at the time
        if boolean_select_most_active == 1 and not select_most_active_completed:
            trainer.set_most_active_channels(args.d_c_f)
            saved.discriminative_channel_indices = trainer.model.discriminative_channel_indices
            saved.metrics.append({
                "stage": "selecting_most_active_channels",
            })

        # skipping stage 3

        # stage 4 new
        autoencoder_finetune_best_loss = float("inf")
        print("Training Autoencoder Finetune")
        for epoch in range(epochs_autoencoder_finetune_completed, epochs_autoencoder_finetune):
            begin = time.time()
            trainer.model.train()
            train_loss_modified, train_autoencoder_loss, train_loss_addition = trainer.train_autoencoder_new(
                epoch,
                epochs_autoencoder_finetune)

            trainer.model.eval()
            valid_loss_modified, valid_autoencoder_loss, valid_loss_addition = trainer.validate_autoencoder_new(
                epoch,
                epochs_autoencoder_finetune)

            duration = time.time() - begin

            if valid_loss_modified < autoencoder_finetune_best_loss:
                autoencoder_finetune_best_loss = valid_loss_modified
                saved.best_state = {
                    key: value.to("cpu").clone()
                    for key, value in trainer.model.state_dict().items()
                }
            saved.metrics.append({
                "stage": "training_autoencoder_finetune",
                "train total": train_loss_modified,
                "train autoencoder": train_autoencoder_loss,
                "train addition": train_loss_addition,
                "valid total": valid_loss_modified,
                "valid autoencoder": valid_autoencoder_loss,
                "valid addition": valid_loss_addition,
                "best": autoencoder_finetune_best_loss,
                "duration": duration
            })
            json.dump(saved.metrics, open(metrics_path, "w"))
            saved.last_state = trainer.model.state_dict()
            saved.optimizer = trainer.optimizer.state_dict()

            th.save(saved, checkpoint)

            print(f"Epoch {epoch + 1:03d}: "
                  f"train total={train_loss_modified:.8f} "
                  f"train autoencoder={train_autoencoder_loss:.8f} "
                  f"train addition={train_loss_addition:.8f} "
                  f"valid total={valid_loss_modified:.8f} "
                  f"valid autoencoder={valid_autoencoder_loss:.8f} "
                  f"valid addition={valid_loss_addition:.8f} "
                  f"best={autoencoder_finetune_best_loss:.4f} "
                  f"duration={human_seconds(duration)}")

    # the main training process that is the focus of the reported
    else:
        # stage 1
        #initially training the autoencoder
        if epochs_autoencoder_initial > 0:
            autoencoder_initial_best_loss = float("inf")
            print("Training Autoencoder Initial")
            for epoch in range(epochs_autoencoder_initial_completed, epochs_autoencoder_initial):
                begin = time.time()
                trainer.model.train()
                train_loss = trainer.train_autoencoder_initial(epoch, epochs_autoencoder_initial)
                trainer.model.eval()
                valid_loss = trainer.validate_autoencoder_initial(epoch, epochs_autoencoder_initial)

                duration = time.time() - begin

                if valid_loss < autoencoder_initial_best_loss:
                    autoencoder_initial_best_loss = valid_loss
                    saved.best_state = {
                        key: value.to("cpu").clone()
                        for key, value in trainer.model.state_dict().items()
                    }
                saved.metrics.append({
                    "stage": "training_autoencoder_initial",
                    "train": train_loss,
                    "valid": valid_loss,
                    "best": autoencoder_initial_best_loss,
                    "duration": duration
                })
                json.dump(saved.metrics, open(metrics_path, "w"))
                saved.last_state = trainer.model.state_dict()
                saved.optimizer = trainer.optimizer.state_dict()

                th.save(saved, checkpoint)

                print(f"Epoch {epoch + 1:03d}: "
                      f"train={train_loss:.8f} valid={valid_loss:.8f} best={autoencoder_initial_best_loss:.4f} "
                      f"duration={human_seconds(duration)}")

            trainer.model.load_state_dict(saved.best_state)

        # stage 2
        # selecting the most active channels
        # this only needs to run once as it's not training, only determining the most active channels at the time
        if boolean_select_most_active == 1 and not select_most_active_completed:
            trainer.set_most_active_channels(args.d_c_f)
            saved.discriminative_channel_indices = trainer.model.discriminative_channel_indices
            saved.metrics.append({
                "stage": "selecting_most_active_channels",
            })

        # stage 3
        # training the discriminative network
        discriminative_best_loss = float("inf")
        print("Training Discriminative Network")
        for epoch in range(epochs_discriminative_completed, epochs_discriminative):
            begin = time.time()
            trainer.model.train()
            train_loss = trainer.train_discriminative(epoch, epochs_discriminative)
            trainer.model.eval()
            valid_loss = trainer.validate_discriminative(epoch, epochs_discriminative)

            duration = time.time() - begin

            if valid_loss < discriminative_best_loss:
                discriminative_best_loss = valid_loss
                saved.best_state = {
                    key: value.to("cpu").clone()
                    for key, value in trainer.model.state_dict().items()
                }
            saved.metrics.append({
                "stage": "training_discriminative",
                "train": train_loss,
                "valid": valid_loss,
                "best": discriminative_best_loss,
                "duration_discrim": duration
            })
            json.dump(saved.metrics, open(metrics_path, "w"))
            saved.last_state = trainer.model.state_dict()
            saved.optimizer = trainer.optimizer.state_dict()

            th.save(saved, checkpoint)

            print(f"Epoch {epoch + 1:03d}: "
                  f"train={train_loss:.8f} valid={valid_loss:.8f} best={discriminative_best_loss:.4f} "
                  f"duration={human_seconds(duration)}")

        trainer.model.load_state_dict(saved.best_state)

        # stage 4
        # fine tuning the autoencoder and using the discriminative network to force features into certain channels
        autoencoder_finetune_best_loss = float("inf")
        print("Training Autoencoder Finetune")
        for epoch in range(epochs_autoencoder_finetune_completed, epochs_autoencoder_finetune):
            begin = time.time()
            trainer.model.train()
            train_loss_modified, train_autoencoder_loss, train_loss_addition = trainer.train_autoencoder_finetune(
                epoch,
                epochs_autoencoder_finetune)

            trainer.model.eval()
            valid_loss_modified, valid_autoencoder_loss, valid_loss_addition = trainer.validate_autoencoder_finetune(
                epoch,
                epochs_autoencoder_finetune)

            duration = time.time() - begin

            if valid_loss_modified < autoencoder_finetune_best_loss:
                autoencoder_finetune_best_loss = valid_loss_modified
                saved.best_state = {
                    key: value.to("cpu").clone()
                    for key, value in trainer.model.state_dict().items()
                }
            saved.metrics.append({
                "stage": "training_autoencoder_finetune",
                "train total": train_loss_modified,
                "train autoencoder": train_autoencoder_loss,
                "train addition": train_loss_addition,
                "valid total": valid_loss_modified,
                "valid autoencoder": valid_autoencoder_loss,
                "valid addition": valid_loss_addition,
                "best": autoencoder_finetune_best_loss,
                "duration": duration
            })
            json.dump(saved.metrics, open(metrics_path, "w"))
            saved.last_state = trainer.model.state_dict()
            saved.optimizer = trainer.optimizer.state_dict()

            th.save(saved, checkpoint)

            print(f"Epoch {epoch + 1:03d}: "
                  f"train total={train_loss_modified:.8f} "
                  f"train autoencoder={train_autoencoder_loss:.8f} "
                  f"train addition={train_loss_addition:.8f} "
                  f"valid total={valid_loss_modified:.8f} "
                  f"valid autoencoder={valid_autoencoder_loss:.8f} "
                  f"valid addition={valid_loss_addition:.8f} "
                  f"best={autoencoder_finetune_best_loss:.4f} "
                  f"duration={human_seconds(duration)}")

    # load the best validation parameters
    trainer.model.load_state_dict(saved.best_state)

    # save the model
    th.save(trainer.model, args.models / f"{name}.th")

    # print some info to the user
    total_duration = time.time() - total_begin
    print(f"total duration={human_seconds(total_duration)}")
    print("done")


if __name__ == "__main__":
    main()